#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "util.h"
#include "stretchy_buffer.h"
#include "xxhash.h"

strpool* strpool_init()
{
    strpool* p = malloc(sizeof(strpool));
    memset(p, 0, sizeof(strpool));
    return p;
}

uint16_t strpool_push(strpool* pool, const char* str, size_t len)
{
    uint8_t* dst = sb_add(pool->buf, len+1);
    memcpy(dst, str, len);
    dst[len] = 0;
    sb_push(pool->offsets, (uint16_t)(dst - pool->buf));
    return sb_count(pool->offsets)-1;
}

const char* strpool_get(strpool* pool, uint16_t hnd)
{
    return &pool->buf[pool->offsets[hnd]];
}

uint16_t strpool_concat(strpool* pool, uint16_t hnd_a, uint16_t hnd_b)
{
    size_t len_a = pool->offsets[hnd_a+1] - pool->offsets[hnd_a];
    size_t len_b = pool->offsets[hnd_b+1] - pool->offsets[hnd_b];
    char concat[len_a+len_b];
    memcpy(concat, pool->buf + pool->offsets[hnd_a], len_a);
    memcpy(concat + len_a, pool->buf + pool->offsets[hnd_b], len_b);
    return strpool_push(pool, concat, len_a+len_b);
}

void strpool_free(strpool* pool)
{
    sb_free(pool->buf);
    sb_free(pool->offsets);
    free(pool);
}

typedef struct Dict
{
    void**   keys;
    void** values;
    size_t count;
    size_t limit;
} Dict;

Dict* dict_init(size_t limit)
{
    Dict* d = malloc(sizeof(Dict));
    memset(d, 0, sizeof(Dict));
    d->keys = malloc(sizeof(void*) * limit);
    d->values = malloc(sizeof(void*) * limit);
    memset(d->keys, 0, limit * sizeof(void*));
    memset(d->values, 0, limit * sizeof(void*));
    d->limit = limit;
}

void dict_put(Dict* d, const char* key, void* value)
{
    uint64_t hash = XXH64(key, strlen(key), 0xdeadc0de);
    uint32_t pos = hash % d->limit;

    void* k = d->keys[pos];
    uint64_t entry_hash;
    while (k != NULL) {
        if (strcmp(k, key) == 0) // Do not duplicate same keys
            break;
        pos = (pos+1) % d->limit;
        k = d->keys[pos];
    }

    d->keys[pos] = (void*)key;
    d->values[pos] = value;
    d->count++;
}

void** dict_values_array(Dict* d)
{
    void** values = NULL;
    for (int i=0; i<d->limit; i++) 
    {
        if (d->keys[i] != NULL)
        {
            sb_push(values, d->values[i]);
        }
    }
    return values;
}

void** dict_keys_array(Dict* d)
{
    void** keys = NULL;
    for (int i=0; i<d->limit; i++) 
    {
        if (d->keys[i] != NULL)
        {
            sb_push(keys, d->keys[i]);
        }
    }
    return keys;
}

void* dict_get(Dict* d, const char* key)
{
    if (d->count == 0)
        return NULL;
    uint64_t hash = XXH64(key, strlen(key), 0xdeadc0de);
    uint32_t pos = hash % d->limit;
    
    void* k = d->keys[pos];
    if (k == NULL)
        return NULL;

    uint64_t khash = XXH64(k, strlen(k), 0xdeadc0de);
    if (khash == hash)
        return d->values[pos];

    // Keep searching (linear probing)
    while (1) {
        pos = (pos+1) % d->limit; // Wrap around
        k = d->keys[pos];
        if (k == NULL)
            break;

        khash = XXH64(k, strlen(k), 0xdeadc0de);
        if (strcmp(k, key) == 0)
            return d->values[pos];
    }

    return NULL;
}

void dict_free(Dict* d)
{
    free(d->keys);
    free(d->values);
    free(d);
}

void dict_print(Dict* d)
{
    printf("Dictionary entries:\n");
    for (int i=0; i<d->limit; i++) {
        if (d->keys[i] != NULL) {
            uint64_t hash = XXH64(d->keys[i], strlen(d->keys[i]), 0xdeadc0de);
            int diff = i - (hash % d->limit);
            printf("Key: %s ||| Value: %p ||| Diff:%d\n", (char*)d->keys[i], d->values[i], diff);
        }
    }
}
