#ifndef YUMMY_UTIL_H_INCLUDED
#define YUMMY_UTIL_H_INCLUDED

typedef struct strpool {
    uint8_t*    buf;
    uint16_t*   offsets;
} strpool; 

strpool* strpool_init();
void strpool_free(strpool* pool);
uint16_t    strpool_push(strpool* pool, const char* str, size_t len);
const char* strpool_get(strpool* pool, uint16_t hnd);
uint16_t strpool_concat(strpool* pool, uint16_t hnd_a, uint16_t hnd_b);

#define PUSH_STR(buf, str) \
    push_len = strlen(str); \
    push_str = sb_add(buf,push_len); \
    memcpy(push_str, str, push_len)

typedef struct Dict Dict;
Dict* dict_init(size_t limit);
void dict_put(Dict* d, const char* key, void* value);
void* dict_get(Dict* d, const char* key);
void dict_print(Dict* d);
void dict_free(Dict* d);
void** dict_values_array(Dict* d);

#endif
