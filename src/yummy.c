#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#include "stretchy_buffer.h"
#include "xxhash.h"
#include "util.h"

typedef uint8_t  uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;
typedef int8_t   int8;
typedef int16_t  int16;
typedef int32_t  int32;
typedef int64_t  int64;

typedef float  float32;
typedef double float64;

#define DECL_BUF(BufName, Size) \
   char* BufName = malloc(Size); \
   uint32 Offset##BufName = 0; \
   memset(BufName, 0, Size)

#define WRITE_BUF(BufName, Str) \
   memcpy(BufName + Offset##BufName, Str, strlen(Str)); \
   Offset##BufName += strlen(Str)

#define WRITE_BUFN(BufName, Str, Len) \
   memcpy(BufName + Offset##BufName, Str, Len); \
   Offset##BufName += Len

enum {
    TOKEN_FREE,
    TOKEN_SEXP_START,
    TOKEN_SEXP_END,
    TOKEN_SYMBOL_START,
    TOKEN_SYMBOL_END,
    TOKEN_INT_START,
    TOKEN_INT_END,
    TOKEN_STRING_START,
    TOKEN_STRING_END,
    TOKEN_CINLINE_START,
    TOKEN_CINLINE_END,
    TOKEN_QUOTE,
    TOKEN_FLOAT_START,
    TOKEN_FLOAT_END,
    NUM_TOKEN_TYPES,
};

enum {
    VALUE_NIL,
    VALUE_F64,
    VALUE_I64,
    VALUE_SEXP,
    VALUE_STRING,
    VALUE_SYMBOL,
    VALUE_CINLINE,
};
typedef struct token
{
    uint8 Type;
    uint32 Loc;
} token;

typedef struct value
{
   union {
   uint64 Hnd;
   char*  Ptr;
   double F64;
   int64  I64;
   };
   uint8 Type;
} value;

typedef struct sexp_node sexp_node;
typedef struct dict dict;
typedef struct sexp_node
{
   value       Value;
   sexp_node** Children;
} sexp_node;

typedef struct compiler compiler;
typedef struct module
{
   char*            Name;
   sexp_node*       RootNode;
   Dict*            Macros;
   Dict*            Functions;
   struct module**  Imports;
   sexp_node*       Nodes;
   uint32           NodesSize;
   char*            FuncDefs;
   char*            FuncDecls;
   char*            StructDecls;
   char*            StructDefs;
   char*            InlineCode;
   char*            HeaderCode;
   size_t           OffsetFuncDefs;
   size_t           OffsetFuncDecls;
   size_t           OffsetStructDecls;
   size_t           OffsetStructDefs;
   size_t           OffsetInlineCode;
   compiler*        Compiler;
} module;

typedef struct compiler
{
    module**    Modules;
    char*       Symbols;
    size_t      SymbolsSize;
    char*       ModulePath; // Directory to search modules
} compiler;
static compiler *s_Compiler;

#define PUSH_TOKEN(token, arr, t, l) \
    token.Type = t; \
    token.Loc = l; \
    sb_push(arr, token)

/* Parses the Tokens and returns a token stream for the input string */
static token* ParseTokens(const char* Source, size_t SourceLen, size_t* SexpLen)
{
    token* Tokens = NULL;
    int SexpCnt = 0;
    token Token;
    Token.Type = TOKEN_FREE;
    Token.Loc  = 0;
    sb_push(Tokens, Token);

    uint32 CinlineCnt = 0; // For tracking # of occurences of '[' and ']' inside inline bLocks
    for (uint32 i = 0; i < SourceLen-1; i++)
    {
        uint8 LastTokenType = sb_last(Tokens).Type;
        switch (Source[i])
        {
            case '(':
                // End the symbol or the number if they were being parsed - symbols cannot contain ()
                if (LastTokenType == TOKEN_SYMBOL_START) {
                printf("Error parsing token %.*s. Symbols cannot contain ()\n", i-sb_last(Tokens).Loc, Source+i);
                    return NULL;
                }
                else if (LastTokenType == TOKEN_INT_START || LastTokenType == TOKEN_FLOAT_START) {
                    printf("Error parsing token %.*s. Symbols cannot contain ()\n", i-sb_last(Tokens).Loc, Source+i);
                    return NULL;
                }
                // If the last token was free, start a new SEXP
                else if (LastTokenType == TOKEN_FREE) {
                    PUSH_TOKEN(Token, Tokens, TOKEN_SEXP_START, i);
                    PUSH_TOKEN(Token, Tokens, TOKEN_FREE, i); 
                    SexpCnt++;
                }
                break;
            case ')':
                // End the symbol or the number if they were being parsed - symbols cannot contain ()
                if (LastTokenType == TOKEN_SYMBOL_START) {
                    PUSH_TOKEN(Token, Tokens, TOKEN_SYMBOL_END, i); 
                    PUSH_TOKEN(Token, Tokens, TOKEN_SEXP_END, i);
                    PUSH_TOKEN(Token, Tokens, TOKEN_FREE, i); 
                    SexpCnt--;
                }
                else if (LastTokenType == TOKEN_INT_START) {
                    PUSH_TOKEN(Token, Tokens, TOKEN_INT_END, i); 
                    PUSH_TOKEN(Token, Tokens, TOKEN_SEXP_END, i);
                    PUSH_TOKEN(Token, Tokens, TOKEN_FREE, i); 
                    SexpCnt--;
                }
                else if (LastTokenType == TOKEN_FLOAT_START) {
                    PUSH_TOKEN(Token, Tokens, TOKEN_FLOAT_END, i); 
                    PUSH_TOKEN(Token, Tokens, TOKEN_SEXP_END, i);
                    PUSH_TOKEN(Token, Tokens, TOKEN_FREE, i); 
                    SexpCnt--;
                }
                else if (LastTokenType == TOKEN_FREE) {
                    PUSH_TOKEN(Token, Tokens, TOKEN_SEXP_END, i);
                    PUSH_TOKEN(Token, Tokens, TOKEN_FREE, i); 
                    SexpCnt--;
                }
                if (SexpCnt == 0)
                {
                   *SexpLen = i;
                   return Tokens;
                }
                break;
            case '[':
                // End the symbol or the number if they were being parsed - symbols cannot contain ()
                if (LastTokenType == TOKEN_SYMBOL_START) {
                printf("Error parsing token %.*s. Symbols cannot contain [\n", i-sb_last(Tokens).Loc, Source+i);
                    return NULL;
                }
                else if (LastTokenType == TOKEN_INT_START || LastTokenType == TOKEN_FLOAT_START) {
                    printf("Error parsing token %.*s. Numbers cannot contain ]\n", i-sb_last(Tokens).Loc, Source+i);
                    return NULL;
                }
                // If the last token was free, start a new SEXP
                else if (LastTokenType == TOKEN_FREE) {
                    PUSH_TOKEN(Token, Tokens, TOKEN_CINLINE_START, i);
                    CinlineCnt = 1;
                }
                else if (LastTokenType == TOKEN_CINLINE_START) {
                   CinlineCnt++;
                }
                break;
            case ']':
                if (LastTokenType == TOKEN_CINLINE_START) {
                   if (CinlineCnt == 1) {
                    PUSH_TOKEN(Token, Tokens, TOKEN_CINLINE_END, i);
                    PUSH_TOKEN(Token, Tokens, TOKEN_FREE, i); 
                   }
                   CinlineCnt--;
                }
                break;
            case '\'':
                if (LastTokenType == TOKEN_FREE) {
                    PUSH_TOKEN(Token, Tokens, TOKEN_QUOTE, i); 
                    PUSH_TOKEN(Token, Tokens, TOKEN_FREE, i); 
                }
                break;
            case ' ':
            case '\n':
            case '\t':
                if (LastTokenType == TOKEN_SYMBOL_START) {
                    PUSH_TOKEN(Token, Tokens, TOKEN_SYMBOL_END, i); 
                }
                else if (LastTokenType == TOKEN_INT_START) {
                    PUSH_TOKEN(Token, Tokens, TOKEN_INT_END, i); 
                }
                else if (LastTokenType == TOKEN_FLOAT_START) {
                    PUSH_TOKEN(Token, Tokens, TOKEN_FLOAT_END, i); 
                }
                if (LastTokenType != TOKEN_CINLINE_START && 
                      LastTokenType != TOKEN_STRING_START && LastTokenType != TOKEN_FREE) {
                    PUSH_TOKEN(Token, Tokens, TOKEN_FREE, i); 
                }
                break;
            case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '-':
                if (LastTokenType == TOKEN_FREE) {
                    PUSH_TOKEN(Token, Tokens, TOKEN_INT_START, i); 
                }
                break;
            case '\"':
                if (LastTokenType == TOKEN_FREE) {
                    PUSH_TOKEN(Token, Tokens, TOKEN_STRING_START, i); 
                }
                else if (LastTokenType == TOKEN_STRING_START) {
                    PUSH_TOKEN(Token, Tokens, TOKEN_STRING_END, i); 
                    PUSH_TOKEN(Token, Tokens, TOKEN_FREE, i); 
                }
                break;
            case '.':
                if (LastTokenType == TOKEN_INT_START)
                {
                   sb_last(Tokens).Type = TOKEN_FLOAT_START;
                   LastTokenType = TOKEN_FLOAT_START;
                }
                break;
            default:
                if (LastTokenType == TOKEN_FREE) {
                    PUSH_TOKEN(Token, Tokens, TOKEN_SYMBOL_START, i); 
                }
                break;
        }
    }

    // If the SEXP not enclosed, return NULL
    *SexpLen = 0;
    return NULL;
}

static void PrintSexp(sexp_node* Sexp, int Level)
{
   char Indent[Level*4+1];
   memset(Indent, ' ', sizeof(Indent));
   Indent[Level*4] = 0;
   for (int i=0; i<sb_count(Sexp->Children); i++)
   {
      printf("%s", Indent); 
      sexp_node* Node = Sexp->Children[i];
      switch (Node->Value.Type)
      {
         case VALUE_F64:
            printf("F64: %f\n", Node->Value.F64); break;
         case VALUE_SYMBOL:
            printf("SYM: %s\n", Node->Value.Ptr); break;
         case VALUE_STRING:
            printf("STR: %s\n", Node->Value.Ptr); break;
         case VALUE_CINLINE:
            printf("CIN: %s\n", Node->Value.Ptr); break;
         case VALUE_I64:
            printf("I64: %ld\n", Node->Value.I64); break;
         case VALUE_NIL:
            printf("NIL\n");  break;
         case VALUE_SEXP:
            printf("SXP:\n"); PrintSexp(Node, Level+1); break;
      }
   }
}

sexp_node* EvalSexp(module* Module, sexp_node* Sexp);
enum { 
   HELPER_DEFMACRO, 
   HELPER_DEFUN, 
   HELPER_CASE, 
   HELPER_CAPTURE,
   HELPER_CAPTUREN,
   HELPER_AND,
   HELPER_OR,
   HELPER_LIST,
   HELPER_CIN,
   HELPER_STRUCT,
   HELPER_FOR,
   HELPER_IMPORT,
   HELPER_FOREACH,
   HELPER_MODULE,
   HELPER_PRINT,
   HELPER_HEADER,
   HELPER_FUNC_COUNT 
};

static const char* s_helperStrings[HELPER_FUNC_COUNT] = 
{ 
   "defmacro",
   "defun",
   "case",
   "capture",
   "capture*",
   "and",
   "or",
   "list",
   "cin",
   "struct",
   "for",
   "import",
   "foreach",
   "module",
   "print",
   "header",
};

static uint64_t s_helperHashes[HELPER_FUNC_COUNT];
static void InitHelperHashes() {
   for (int i=0; i<HELPER_FUNC_COUNT; i++) {
      s_helperHashes[i] = XXH64(s_helperStrings[i], strlen(s_helperStrings[i]), 0xdeadc0de);
   }
}

static uint8 FindHelper(const char* Name)
{
   uint64 Hash = XXH64(Name, strlen(Name), 0xdeadc0de);
   for (int i=0; i<HELPER_FUNC_COUNT; i++) {
      if (s_helperHashes[i] == Hash)
         return i;
   }
   return HELPER_FUNC_COUNT;
}

compiler* InitCompiler(size_t SymbolsSize)
{
   InitHelperHashes();
   compiler* Compiler = malloc(sizeof(compiler));
   memset(Compiler, 0, sizeof(compiler));
   Compiler->Symbols = malloc(SymbolsSize);
   memset(Compiler->Symbols, 0, SymbolsSize);
   
   return Compiler;
}

void FreeModule(module* Module);
void FreeCompiler(compiler* Compiler)
{
   for (int i=0; i<sb_count(Compiler->Modules); i++)
   {
      FreeModule(Compiler->Modules[i]);
   }
   sb_free(Compiler->Modules);
   free(Compiler->Symbols);
   free(Compiler);
}

void CompilerModulePath(compiler* Compiler, char* ModulePath)
{
   Compiler->ModulePath = ModulePath;
}

module* InitModule(compiler* Compiler, uint32 MaxNodes)
{
   module* Module = malloc(sizeof(module));
   memset(Module, 0, sizeof(module));
   Module->Compiler = Compiler;
   Module->Macros = dict_init(8 * 1024);
   Module->Nodes = malloc(MaxNodes * sizeof(sexp_node));
   memset(Module->Nodes, 0, MaxNodes * sizeof(sexp_node));
   Module->NodesSize = 0;
   Module->FuncDefs = malloc(32 * 1024);
   Module->FuncDecls = malloc(2 * 1024);
   Module->StructDefs = malloc(32 * 1024);
   Module->StructDecls = malloc(2 * 1024);
   Module->Functions = dict_init(8 * 1024);
   Module->HeaderCode = malloc(2 * 1024);
   return Module;
}

void FreeModule(module* Module)
{
   for (int i=0; i<Module->NodesSize; i++)
   {
      sb_free(Module->Nodes[i].Children);
   }
   sb_free(Module->Imports);
   dict_free(Module->Macros);
   dict_free(Module->Functions);
   free(Module->Nodes);
   free(Module->FuncDefs);
   free(Module->FuncDecls);
   free(Module->StructDecls);
   free(Module->StructDefs);
   if (Module->InlineCode)
   {
      free(Module->InlineCode);
   }
   free(Module->HeaderCode);
   free(Module);
}

// Sets the module name 
void OpModule(module* Module, sexp_node* Sexp)
{
   Module->Name = Sexp->Children[1]->Value.Ptr;
}

int LoadFile(const char *Filename, char** Data, size_t* SizeOut)
{
    size_t size = 0;
    FILE *f = fopen(Filename, "rb");
    if (f == NULL)
    {
        *Data = NULL;
        return -1; // -1 means file opening fail 
    }
    fseek(f, 0, SEEK_END);
    size = ftell(f);
    fseek(f, 0, SEEK_SET);
    *Data = (char *)malloc(size + 1);
    if (size != fread(*Data, sizeof(char), size, f))
    {
        free(*Data);
        return -2; // -2 means file reading fail 
    }
    fclose(f);
    (*Data)[size] = 0;
    *SizeOut = size;
    return 0; // SUCCESS
}

char* CreateSymbol(compiler* Compiler, const char* Str, size_t StrLen)
{
   char* Symbol = Compiler->Symbols + Compiler->SymbolsSize;
   memcpy(Compiler->Symbols + Compiler->SymbolsSize, Str, StrLen);
   Compiler->SymbolsSize += StrLen;
   Compiler->Symbols[Compiler->SymbolsSize++] = 0;
   return Symbol;
}

void AppendChild(sexp_node* Parent, sexp_node* Child)
{
   sb_push(Parent->Children, Child);
}

sexp_node* AllocNode(module* Module, uint8 Type)
{
   sexp_node* Node = &Module->Nodes[Module->NodesSize++];
   Node->Value.Type = Type;
   return Node;
}

void AppendNode(sexp_node* Parent, sexp_node* Suffix)
{
   for (int i=0; i<sb_count(Suffix->Children); i++)
   {
      sb_push(Parent->Children, Suffix->Children[i]);
   }
}

sexp_node* CopyNode(module* Module, sexp_node* Sexp)
{
   if (Sexp->Children)
   {
      sexp_node* Copy = AllocNode(Module, VALUE_SEXP);
      for (int i=0; i<sb_count(Sexp->Children); i++)
      {
         sexp_node* CopiedChild = NULL;
         sexp_node* Child = Sexp->Children[i];
         if (Child->Value.Type == VALUE_SEXP)
         {
            CopiedChild = CopyNode(Module, Child);
            sb_push(Copy->Children, CopiedChild);
         }
         else
         {
            CopiedChild = AllocNode(Module, Child->Value.Type);
            CopiedChild->Value = Child->Value;
            sb_push(Copy->Children, CopiedChild);
         }
      }
      return Copy;
   }
   else
   {
      sexp_node* Copy = AllocNode(Module, Sexp->Value.Type);
      Copy->Value = Sexp->Value;
      return Copy;
   }
}

sexp_node* FindMacro(module* Module, const char* Name);
void* FindFunction(module* Module, const char* Name);
enum { EXEC_STOP, EXEC_HELPER, EXEC_MACRO, EXEC_RECURSE, EXEC_FUNCALL};
uint8 SexpLogic(module* Module, sexp_node* Sexp)
{
   if (sb_count(Sexp->Children) == 0)
   {
      return EXEC_STOP;
   }

   sexp_node* Op = Sexp->Children[0];
   if(Op->Value.Type == VALUE_SYMBOL)
   {
      const char* SymbolStr = Op->Value.Ptr;
      uint8 Helper = FindHelper(SymbolStr);
      if (Helper != HELPER_FUNC_COUNT)
         return EXEC_HELPER;

      sexp_node* Macro = FindMacro(Module, SymbolStr);
      if (Macro) 
      {
         return EXEC_MACRO;
      }

      void* FuncExists = FindFunction(Module, SymbolStr);

      if (FuncExists)
      {
         return EXEC_FUNCALL;
      }

   }
   return EXEC_RECURSE;
}

sexp_node* RewriteSexp(module* Module, sexp_node* Sexp);

sexp_node* OpCin(module* Module, sexp_node* CinSexp)
{
}

/*
#(defun void print-number ((int t))
    [
    printf("Printing number %d", t);
    ])
    */

sexp_node* ExpandMacro(module* Module, sexp_node* Sexp, Dict* Args);

/* Expected SEXP:
 * (foreach elem list
 *     Body-Of-The-Generated-Loop)
 *
 * Loops over the entire "list" without recursion, replaces every occurence of symbol "elem" inside body with
 * elements from the list for each loop iteration.
 * */
sexp_node* OpForeach(module* Module, sexp_node* ForSexp)
{
   sexp_node* Elem = ForSexp->Children[1]; // Expected: Symbol
   sexp_node* List = ForSexp->Children[2]; // Expected: List

   uint32 LenList = sb_count(List->Children);
   uint32 LenSexp = sb_count(ForSexp->Children);
   sexp_node* Out = AllocNode(Module, VALUE_SEXP);
   for (int i=0; i<LenList; i++)
   {
      Dict* Replacements = dict_init(2);
      dict_put(Replacements, Elem->Value.Ptr, List->Children[i]);
      for (int ii=3; ii<LenSexp; ii++)
      {
         sexp_node* Generated = ExpandMacro(Module, ForSexp->Children[ii], Replacements);
         sexp_node* Rewrite   = EvalSexp(Module, Generated);
         AppendNode(Out, Rewrite);
      }
      dict_free(Replacements);
   }
   return Out;
}

/* Expected SEXP:
 * (for iteration_elems list body)
 * (for (iterator_0 iterator_1) ((a0 a1) (b0 b1))
 *     Body-Of-The-Generated-Loop)
 *
 * Loops over the list without recursion, generating the code inside body. It captures elements of elements of the list(a0 a1 ...) and
 * replaces every occurence of the iterator variables with elements of elements of the list.
 * */

sexp_node* OpFor(module* Module, sexp_node* ForSexp)
{
   sexp_node* Iterator = ForSexp->Children[1];
   sexp_node* Iterated = ForSexp->Children[2];

   if (Iterator->Value.Type == VALUE_SEXP)
   {
      Iterator = EvalSexp(Module, Iterator);
   }
   if (Iterated->Value.Type == VALUE_SEXP)
   {
      Iterated = EvalSexp(Module, Iterated);
   }
   sexp_node* Out = AllocNode(Module, VALUE_SEXP);

   for (int i=0; i<sb_count(Iterated->Children); i++)
   {
      Dict* Replacements = dict_init(sb_count(Iterator->Children) * 2);
      uint32 NumIterators = sb_count(Iterator->Children);
      for (int ii=0; ii<NumIterators; ii++)
      {
         dict_put(Replacements, Iterator->Children[ii]->Value.Ptr, 
               Iterated->Children[i]->Children[ii]);
      }

      uint32 NumForChildren = sb_count(ForSexp->Children);
      for (int j=3; j<NumForChildren; j++)
      {
         sexp_node* Rewrite = ForSexp->Children[j];
         sexp_node* Generated = ExpandMacro(Module, Rewrite, Replacements);
         Rewrite = EvalSexp(Module, Generated);
         sb_push(Out->Children, Rewrite);
      }
      dict_free(Replacements);
   }
   return Out;
}

module* LoadModuleSource(compiler* Compiler, const char* Source, size_t SourceLen);
void OpImport(module* Module, sexp_node* Import)
{
   module** ModulesArray = Module->Compiler->Modules;
   const char* ModuleName = Import->Children[1]->Value.Ptr;

   if (Module->Name == NULL)
   {
      printf("Error: Trying to load module %s into undefined module\n", ModuleName);
      return;
   }
   for (int i=0; i < sb_count(ModulesArray); i++)
   {
      if (strcmp(ModulesArray[i]->Name, ModuleName) == 0)
      {
         sb_push(Module->Imports, ModulesArray[i]);
         return;
      }
   }

   char Filename[64];
   snprintf(Filename, 64, "%s/%s.ym", Module->Compiler->ModulePath, ModuleName);

   char* Source;
   size_t SourceLen, SexpLen;
   LoadFile(Filename, &Source, &SourceLen);

   if (!Source)
   {
      printf("Error: Couldn't import module from file %s\nFile does not exist.\n", Filename);
      return;
   }

   module* LoadedModule = LoadModuleSource(Module->Compiler, Source, SourceLen);
   free(Source);

   if (LoadedModule)
   {
      sb_push(Module->Imports, LoadedModule);
      return;
   }

   printf("In module %s: Couldn't load module %s\n", Module->Name, ModuleName);
}

static void OpPrint(module* Module, sexp_node* Sexp)
{
   for (int i=1; i<sb_count(Sexp->Children); i++)
   {
      sexp_node* Node = AllocNode(Module, VALUE_SEXP);
      AppendChild(Node, Sexp->Children[i]);
      sexp_node* Evaluated = EvalSexp(Module, Node);
      switch (Evaluated->Value.Type)
      {
         case VALUE_F64:
            printf("F64: %f\n", Evaluated->Value.F64); break;
         case VALUE_SYMBOL:
            printf("SYM: %s\n", Evaluated->Value.Ptr); break;
         case VALUE_STRING:
            printf("STR: %s\n", Evaluated->Value.Ptr); break;
         case VALUE_CINLINE:
            printf("CIN: %s\n", Evaluated->Value.Ptr); break;
         case VALUE_I64:
            printf("I64: %ld\n", Evaluated->Value.I64); break;
         case VALUE_SEXP:
            PrintSexp(Evaluated, 0);
      }
   }
}

void OpDefMacro(module* Module, sexp_node* MacroSexp);
void OpStruct(module* Module, sexp_node* Struct)
{
   char* StructName = Struct->Children[1]->Value.Ptr;
   
   DECL_BUF(StructDecl, 256);
   DECL_BUF(StructDef, 8*1024);

   WRITE_BUF(StructDecl, "typedef struct ");
   WRITE_BUF(StructDecl, StructName);
   WRITE_BUF(StructDecl, "_AOS;\n ");

   WRITE_BUF(StructDecl, "typedef struct ");
   WRITE_BUF(StructDecl, StructName);
   WRITE_BUF(StructDecl, "_SOA;\n ");

   WRITE_BUF(StructDef, "struct ");
   WRITE_BUF(StructDef, StructName);
   WRITE_BUF(StructDef, "_AOS\n{\n");
   for (int i=2; i<sb_count(Struct->Children); i++)
   {
      WRITE_BUF(StructDef, Struct->Children[i]->Children[0]->Value.Ptr); // Type of field
      WRITE_BUFN(StructDef, " ", 1);
      WRITE_BUF(StructDef, Struct->Children[i]->Children[1]->Value.Ptr); // Name of field
      WRITE_BUF(StructDef, ";\n");
   }
   WRITE_BUF(StructDef, "};\n ");

   WRITE_BUF(StructDef, "struct ");
   WRITE_BUF(StructDef, StructName);
   WRITE_BUF(StructDef, "_SOA\n{\n");
   for (int i=2; i<sb_count(Struct->Children); i++)
   {
      WRITE_BUF(StructDef, Struct->Children[i]->Children[0]->Value.Ptr); // Type of field
      WRITE_BUF(StructDef, "* ");
      WRITE_BUF(StructDef, Struct->Children[i]->Children[1]->Value.Ptr); // Name of field
      WRITE_BUF(StructDef, ";\n");
   }
   WRITE_BUF(StructDef, "uint32 size;\nuint32 count;\n};\n ");

   size_t len = snprintf(Module->StructDefs + Module->OffsetStructDefs, OffsetStructDef, "%s\n", StructDef);
   *(Module->StructDefs + Module->OffsetStructDefs + len-2) = ' '; // Because snprintf puts a NULL terminator
   Module->OffsetStructDefs += OffsetStructDef-1;

   len = snprintf(Module->StructDecls + Module->OffsetStructDecls, OffsetStructDecl, "%s\n", StructDecl);
   *(Module->StructDecls + Module->OffsetStructDecls + len-2) = ' ';
   Module->OffsetStructDecls += OffsetStructDecl-1;

   sexp_node* StructFieldsMacro = AllocNode(Module, VALUE_SEXP);
   sexp_node* Field = AllocNode(Module, VALUE_SYMBOL);
   Field->Value.Ptr = CreateSymbol(Module->Compiler, "defmacro", 8);
   AppendChild(StructFieldsMacro, Field);

   DECL_BUF(MacroName, 64);
   WRITE_BUF(MacroName, StructName);
   WRITE_BUF(MacroName, "-Fields");
   Field = AllocNode(Module, VALUE_SYMBOL);
   Field->Value.Ptr = CreateSymbol(Module->Compiler, MacroName, strlen(MacroName));
   AppendChild(StructFieldsMacro, Field);
   Field = AllocNode(Module, VALUE_SEXP);
   AppendChild(StructFieldsMacro, Field);
   for (int i=2; i<sb_count(Struct->Children); i++)
   {
      AppendChild(StructFieldsMacro, Struct->Children[i]);
   }

   OpDefMacro(Module, StructFieldsMacro);

   free(StructDecl);
   free(StructDef);
   free(MacroName);
}

void OpDefun(module* Module, sexp_node* FunSexp)
{
   sexp_node* ReturnVals = FunSexp->Children[1];
   const char* FuncName = FunSexp->Children[2]->Value.Ptr;
   sexp_node* Arguments = FunSexp->Children[3];
   sexp_node* Body = FunSexp->Children[4];

   uint32 NumArguments = sb_count(Arguments->Children);
   
   DECL_BUF(FuncDef, 8*1024);
   DECL_BUF(FuncDecl, 8*1024);

   WRITE_BUF(FuncDef, ReturnVals->Value.Ptr);
   WRITE_BUF(FuncDef, " ");

   WRITE_BUF(FuncDef, FuncName);
   WRITE_BUF(FuncDef, "(");
   if (NumArguments > 0)
   {
      for (int i=0; i<NumArguments-1; i++)
      {
         WRITE_BUF(FuncDef, Arguments->Children[i]->Children[0]->Value.Ptr);
         WRITE_BUF(FuncDef, " ");
         WRITE_BUF(FuncDef, Arguments->Children[i]->Children[1]->Value.Ptr);
         WRITE_BUF(FuncDef, ", ");
      }
      WRITE_BUF(FuncDef, Arguments->Children[NumArguments-1]->Children[0]->Value.Ptr);
      WRITE_BUF(FuncDef, " ");
      WRITE_BUF(FuncDef, Arguments->Children[NumArguments-1]->Children[1]->Value.Ptr);
   }
   WRITE_BUF(FuncDef, ")\n{");
   WRITE_BUF(FuncDef, Body->Value.Ptr);
   WRITE_BUF(FuncDef, "}\n ");

   size_t len = snprintf(Module->FuncDefs + Module->OffsetFuncDefs, OffsetFuncDef, "%s\n", FuncDef);
   *(Module->FuncDefs + Module->OffsetFuncDefs + len-2) = ' ';
   Module->OffsetFuncDefs += OffsetFuncDef-1;

   WRITE_BUF(FuncDecl, ReturnVals->Value.Ptr);
   WRITE_BUF(FuncDecl, " ");

   WRITE_BUF(FuncDecl, FuncName);
   WRITE_BUF(FuncDecl, "(");
   if (NumArguments > 0)
   {
      for (int i=0; i<NumArguments-1; i++)
      {
         WRITE_BUF(FuncDecl, Arguments->Children[i]->Children[0]->Value.Ptr);
         WRITE_BUF(FuncDecl, " ");
         WRITE_BUF(FuncDecl, Arguments->Children[i]->Children[1]->Value.Ptr);
         WRITE_BUF(FuncDecl, ", ");
      }
      WRITE_BUF(FuncDecl, Arguments->Children[NumArguments-1]->Children[0]->Value.Ptr);
      WRITE_BUF(FuncDecl, " ");
      WRITE_BUF(FuncDecl, Arguments->Children[NumArguments-1]->Children[1]->Value.Ptr);
   }
   WRITE_BUF(FuncDecl, ");\n ");

   len = snprintf(Module->FuncDecls + Module->OffsetFuncDecls, OffsetFuncDecl, "%s\n", FuncDecl);
   *(Module->FuncDecls + Module->OffsetFuncDecls + len-2) = ' ';
   Module->OffsetFuncDecls += OffsetFuncDecl-1;

   dict_put(Module->Functions, FuncName, (void*)1);

   free(FuncDef);
   free(FuncDecl);
}
sexp_node* OpList(module* Module, sexp_node* ListSexp)
{
   uint32 NumSexps = sb_count(ListSexp->Children);
   sexp_node* Rewritten, *Reevaluated;
   sexp_node* NewList = AllocNode(Module, VALUE_SEXP);
   sexp_node* Out = AllocNode(Module, VALUE_SEXP);

   for (int i=1; i<NumSexps; i++)
   {
      sexp_node* Child = ListSexp->Children[i];
      if (Child->Value.Type == VALUE_SEXP)
      {
        sexp_node* Rewritten = AllocNode(Module, VALUE_SEXP);
        AppendChild(Rewritten, Child);
        Reevaluated = EvalSexp(Module, Rewritten);
        if (Reevaluated->Children)
           AppendNode(Out, Reevaluated);
      }
      else
      {
         sb_push(Out->Children, Child);
      }
   }
   AppendChild(NewList, Out);
   return NewList;
}

sexp_node* OpAnd(module* Module, sexp_node* AndSexp)
{
   uint32_t sexp_cnt = sb_count(AndSexp->Children);
   sexp_node* Out = AllocNode(Module, VALUE_SEXP);
   sexp_node* EvalChild, *RewriteChild;
   for (int i=1; i<sexp_cnt; i++)
   {
      sexp_node* Child = AndSexp->Children[i];
      if (Child->Value.Type == VALUE_SEXP)
      {
         RewriteChild = RewriteSexp(Module, Child);
         EvalChild = EvalSexp(Module, RewriteChild);
         if (!EvalChild->Children)
         {
            printf("Error: Bad argument to and\n");
            PrintSexp(Child, 0);
            return NULL;
         }
         if (strcmp("false", sb_last(EvalChild->Children)->Value.Ptr) == 0)
         {
            sb_push(Out->Children, sb_last(EvalChild->Children));
            return Out;
         }
      }
      else if (Child->Value.Type == VALUE_SYMBOL || Child->Value.Type == VALUE_STRING)
      {
         if (strcmp("false", Child->Value.Ptr) == 0)
         {
            sb_push(Out->Children, Child);
            return Out;
         }
      }
   }

   sb_push(Out->Children, EvalChild->Children[0]);
   return Out;
}

sexp_node* OpOr(module* Module, sexp_node* OrSexp)
{
   uint32_t sexp_cnt = sb_count(OrSexp->Children);
   sexp_node* Out = AllocNode(Module, VALUE_SEXP);
   sexp_node* EvalChild, *RewriteChild;
   for (int i=1; i<sexp_cnt; i++)
   {
      sexp_node* Child = OrSexp->Children[i];
      if (Child->Value.Type == VALUE_SEXP)
      {
         RewriteChild = RewriteSexp(Module, Child);
         EvalChild = EvalSexp(Module, RewriteChild);
         if (strcmp("true", sb_last(EvalChild->Children)->Value.Ptr) == 0)
         {
            sb_push(Out->Children, sb_last(EvalChild->Children));
            return Out;
         }
      }
      else if (Child->Value.Type == VALUE_SYMBOL || Child->Value.Type == VALUE_STRING)
      {
         if (strcmp("true", Child->Value.Ptr) == 0)
         {
            sb_push(Out->Children, Child);
            return Out;
         }
      }
   }

   sb_push(Out->Children, EvalChild->Children[0]);
   return Out;
}

sexp_node* OpCase(module* Module, sexp_node* CaseSexp)
{
   const char* CaseStr = NULL;
   if (CaseSexp->Children[1]->Value.Type == VALUE_SYMBOL || CaseSexp->Children[1]->Value.Type== VALUE_STRING)
   {
      CaseStr = CaseSexp->Children[1]->Value.Ptr;
   }
   else if(CaseSexp->Children[1]->Value.Type == VALUE_SEXP)
   {
      sexp_node* Expansion = RewriteSexp(Module, CaseSexp->Children[1]);
      sexp_node* Reevaluated = EvalSexp(Module, Expansion);
      CaseStr = Expansion->Children[0]->Value.Ptr;
   }
   else
   {
      return NULL;
   }

   sexp_node* Out = AllocNode(Module, VALUE_SEXP);
   uint32 NumCases = sb_count(CaseSexp->Children);
   int MatchedCase = 0;
   int ElseLoc = 0;

   for (int i=2; i<NumCases; i++)
   {
      sexp_node* CaseI = CaseSexp->Children[i];
      if (strcmp(CaseStr, CaseI->Children[0]->Value.Ptr) == 0)
      {
         for (int j=1; j<sb_count(CaseI->Children); j++)
         {
            sb_push(Out->Children, CaseI->Children[j]);
         }
         return Out;
      }
      else if(strcmp(CaseI->Children[0]->Value.Ptr, "else") == 0)
      {
         ElseLoc = i;
      }
   }

   if (ElseLoc)
   {
      for (int j=1; j<sb_count(CaseSexp->Children[ElseLoc]->Children); j++)
      {
         sb_push(Out->Children, CaseSexp->Children[ElseLoc]->Children[j]);
      }
      return Out;
   }

   return NULL;
}

void OpDefMacro(module* Module, sexp_node* MacroSexp)
{
   const char* Name = MacroSexp->Children[1]->Value.Ptr;
   sexp_node* MacroCopy = CopyNode(Module, MacroSexp);
   dict_put(Module->Macros, Name, MacroCopy);
}

static Dict* MacroBuildArgs(module* Module, const sexp_node* CapturedSexp, const sexp_node* Macro)
{
   uint32_t NumSexpArgs = sb_count(CapturedSexp->Children);
   sexp_node* MacroArgs = Macro->Children[2];
   uint32_t NumMacroArgs = sb_count(MacroArgs->Children);
   Dict* Args = dict_init(NumMacroArgs * 2);

   // Variable capture for Macro arguments
   // Skip the first child node of the CapturedSexp Sexp(sexp)
   for (int i=0; i<NumSexpArgs-1; i++)
   {
      const char* ArgStr = MacroArgs->Children[i]->Value.Ptr;
      dict_put(Args, ArgStr, CapturedSexp->Children[i+1]);
   }

   return Args;
}

void* FindFunction(module* Module, const char* Name)
{
   void* FuncExists = dict_get(Module->Functions, Name);
   if (!FuncExists)
   {
      for (int i=0; i<sb_count(Module->Imports); i++)
      {
         FuncExists = dict_get(Module->Imports[i]->Functions, Name);
         if (FuncExists)
            break;
      }
   }
   return FuncExists;
}

sexp_node* FindMacro(module* Module, const char* Name)
{
   sexp_node* Macro = dict_get(Module->Macros, Name);
   if (!Macro)
   {
      for (int i=0; i<sb_count(Module->Imports); i++)
      {
         Macro = dict_get(Module->Imports[i]->Macros, Name);
         if (Macro)
            break;
      }
   }
   return Macro;
}

/* Returns body of macro replaced with arguments from the Sexp
 * Macro: (defmacro macro_name (arg0_name arg1_name ... argn_name) body)
 * Sexp:  (macro_name arg0 arg1 ... argn)
 * Return expansion of "body" where every symbol with name "arg*_name"
 * is replaced with "arg*" of the "Sexp" in a new Sexp.
 * */
void PushArg(sexp_node* Arg, char* Buf, uint32* BufOffset);
sexp_node* RewriteCin(module* Module, sexp_node* CinSexp, Dict* Args);
sexp_node* ExpandMacro(module* Module, sexp_node* Sexp, Dict* Args)
{
   sexp_node* Out = AllocNode(Module, VALUE_SEXP);
   if(Sexp->Value.Type == VALUE_CINLINE)
   {
      sb_push(Out->Children, RewriteCin(Module, Sexp, Args));
   }
   else
   {
      for (int i=0; i<sb_count(Sexp->Children); i++)
      {
         sexp_node* Child = Sexp->Children[i];
         if (Child->Value.Type == VALUE_SEXP)
         {
            sexp_node* Expanded = ExpandMacro(Module, Child, Args);
            sb_push(Out->Children, Expanded);
         }
         else if(Child->Value.Type == VALUE_SYMBOL)
         {
            const char* SymbolStr = Child->Value.Ptr;
            // If the symbol corresponds to an argument of the macro replace it
            // If not, keep it as is in the expanded form
            sexp_node* Replacement = dict_get(Args, SymbolStr);
            if (Replacement) 
            {
               if (Replacement->Value.Type == VALUE_SEXP)
                  sb_push(Out->Children, Replacement);
               else
                  sb_push(Out->Children, Replacement);
            }
            else 
            {
               sb_push(Out->Children, Child);
            }
         }
         else if(Child->Value.Type == VALUE_CINLINE)
         {
            sb_push(Out->Children, RewriteCin(Module, Child, Args));
         }
         else
         {
            sb_push(Out->Children, Child);
         }
      }
   }

   return Out;
}

sexp_node* OpCaptureN(module* Module, sexp_node* CaptureSexp)
{
   sexp_node* CapturedSymbolsList = CaptureSexp->Children[1];
   sexp_node* VarsToCaptureList = CaptureSexp->Children[2];
   sexp_node* Out = AllocNode(Module, VALUE_SEXP);

   if (sb_count(CapturedSymbolsList->Children) != sb_count(VarsToCaptureList->Children))
   {
      printf("Error:\n");
      PrintSexp(CaptureSexp, 0);
      printf("capture* lists are not same size\n");
      return NULL;
   }
   uint16 TotalCaptures = 0;
   for (int i=0; i<sb_count(CapturedSymbolsList->Children); i++)
   {
      TotalCaptures += sb_count(CapturedSymbolsList->Children[i]->Children);
   }
      
   Dict* Args = dict_init(TotalCaptures * 2);
   for (int i=0; i<sb_count(CapturedSymbolsList->Children); i++)
   {
      sexp_node* CapturedSymbols = CapturedSymbolsList->Children[i];
      sexp_node* VarsToCapture = VarsToCaptureList->Children[i];
      uint16 NumCaptures = sb_count(CapturedSymbols->Children);
      for (int i=0; i< NumCaptures; i++)
      {
         sexp_node* CaptureSymbol = CapturedSymbols->Children[i];
         dict_put(Args, CaptureSymbol->Value.Ptr, VarsToCapture->Children[i]);
      }
   }

   for (int i=3; i<sb_count(CaptureSexp->Children); i++)
   {
      if (CaptureSexp->Children[i]->Value.Type == VALUE_SEXP)
      {
         sexp_node* Expanded = ExpandMacro(Module, CaptureSexp->Children[i], Args);
         sexp_node* Reevaluated = EvalSexp(Module, Expanded);
         if (sb_count(Reevaluated->Children))
         {
            AppendNode(Out, Reevaluated);
         }
      }
      else if (CaptureSexp->Children[i]->Value.Type == VALUE_SYMBOL)
      {
         sexp_node* Replace = dict_get(Args, CaptureSexp->Children[i]->Value.Ptr);
         if (Replace)
         {
            sb_push(Out->Children, Replace);
         }
         else
         {
            sb_push(Out->Children, CaptureSexp->Children[i]);
         }
      }
      else if (CaptureSexp->Children[i]->Value.Type == VALUE_CINLINE)
      {
         sb_push(Out->Children, RewriteCin(Module, CaptureSexp->Children[i], Args));
      }

   }
   
   dict_free(Args);
   return Out;
}

sexp_node* OpCapture(module* Module, sexp_node* CaptureSexp)
{
   sexp_node* CapturedSymbols = CaptureSexp->Children[1];
   sexp_node* VarsToCapture = CaptureSexp->Children[2];
   sexp_node* Out = AllocNode(Module, VALUE_SEXP);

   uint16 NumCaptures = sb_count(CapturedSymbols->Children);
   Dict* Args = dict_init(NumCaptures * 2);
   for (int i=0; i< NumCaptures; i++)
   {
      sexp_node* CaptureSymbol = CapturedSymbols->Children[i];
      dict_put(Args, CaptureSymbol->Value.Ptr, VarsToCapture->Children[i]);
   }

   for (int i=3; i<sb_count(CaptureSexp->Children); i++)
   {
      if (CaptureSexp->Children[i]->Value.Type == VALUE_SEXP)
      {
         sexp_node* Expanded = ExpandMacro(Module, CaptureSexp->Children[i], Args);
         sexp_node* Reevaluated = EvalSexp(Module, Expanded);
         if (sb_count(Reevaluated->Children))
         {
            AppendNode(Out, Reevaluated);
         }
      }
      else if (CaptureSexp->Children[i]->Value.Type == VALUE_SYMBOL)
      {
         sexp_node* Replace = dict_get(Args, CaptureSexp->Children[i]->Value.Ptr);
         if (Replace)
         {
            sb_push(Out->Children, Replace);
         }
         else
         {
            sb_push(Out->Children, CaptureSexp->Children[i]);
         }
      }
      else if (CaptureSexp->Children[i]->Value.Type == VALUE_CINLINE)
      {
         sb_push(Out->Children, RewriteCin(Module, CaptureSexp->Children[i], Args));
      }
         
   }
   
   dict_free(Args);
   return Out;
}

/* If the Sexp is a macro expansion, it needs to be rewritten.
 * The rewritten form is returned in Out, which needs to be copied
 * as some nodes of Out are children of Sexp(which will be freed later and thus invalid),
 * and some nodes of Out are children of the macro body(which will be freed if Out is freed).
 * So, we copy the rewritten form in the end.
 * */

sexp_node* CompileSexp(compiler* Compiler, module* Module, const char* Source, size_t SourceLen, size_t* SexpLen);
/* Rewrites the CIN with arguments from macros or captures */
sexp_node* RewriteCin(module* Module, sexp_node* CinSexp, Dict* Args)
{
   const char* Str = CinSexp->Value.Ptr;
   size_t Len = strlen(Str);
   DECL_BUF(ReplacedStr, 2*1024);
   int LastReplacementLoc = 0;
   for (int i=0; i<Len; i++)
   {
      if (Str[i] == '#' && Str[i+1] == '(')
      {
         WRITE_BUFN(ReplacedStr, Str + LastReplacementLoc, i-LastReplacementLoc); // Write code from before the macro
         int SexpStart = i+2;  // Where this Sexp starts
         i += 2;
         while (Str[i] != ')' && i<Len) { i++; }
         LastReplacementLoc = i+1;
         char* ArgStr = malloc(i - SexpStart+1);
         memset(ArgStr, 0, i-SexpStart);
         memcpy(ArgStr, Str+SexpStart, i-SexpStart);
         ArgStr[i-SexpStart] = 0;
         sexp_node* Replacement = dict_get(Args, ArgStr);
         if (Replacement)
         {
            PushArg(Replacement, ReplacedStr, &OffsetReplacedStr);
         }
         else
         {
            size_t SexpLen;
            sexp_node* Compiled = CompileSexp(Module->Compiler, Module, Str+SexpStart-1, Len, &SexpLen);
            if (Compiled && Compiled->Children[0]->Value.Type == VALUE_CINLINE)
            {
               WRITE_BUF(ReplacedStr, Compiled->Children[0]->Value.Ptr);
            }
            else
            {
               WRITE_BUFN(ReplacedStr, Str + SexpStart - 2, i - SexpStart + 3);
            }
         }
         free(ArgStr);
      }
   }
   WRITE_BUFN(ReplacedStr, Str + LastReplacementLoc, Len-LastReplacementLoc); // Write code from before the macro
   ReplacedStr[OffsetReplacedStr] = 0;
   sexp_node* Rewritten = AllocNode(Module, VALUE_CINLINE);
   Rewritten->Value.Ptr = CreateSymbol(Module->Compiler, ReplacedStr, OffsetReplacedStr);
   free(ReplacedStr);
   return Rewritten;
}
sexp_node* RewriteSexp(module* Module, sexp_node* Sexp)
{
   /* If the node is not a Sexp, return NULL */
   if (sb_count(Sexp->Children) == 0)
   {
      return NULL;
   }

   sexp_node* Out, *Expanded, *Reevaluated;

   const sexp_node* OpNode = Sexp->Children[0];

   if(OpNode->Value.Type == VALUE_SYMBOL)
   {
      const char* MacroName  = OpNode->Value.Ptr;
      const sexp_node* Macro = FindMacro(Module, MacroName);
      // If the SEXP operator is a macro expansion
      if (Macro) 
      {
         Out = AllocNode(Module, VALUE_SEXP);
         uint32_t NumMacroArgs = sb_count(Macro->Children[2]->Children);
         uint32_t NumMacroBodySexps = sb_count(Macro->Children) - 3;
         Dict* Args = MacroBuildArgs(Module, Sexp, Macro);

         for (int i=0; i < NumMacroBodySexps; i++)
         {
            sexp_node* Child = Macro->Children[i+3];
            if (Child->Value.Type == VALUE_SEXP)
            {
               Expanded = ExpandMacro(Module, Child, Args);
               if(Expanded->Value.Type == VALUE_CINLINE)
               {
                  sb_push(Out->Children, RewriteCin(Module, Expanded, Args));
               }
               else
               sb_push(Out->Children, Expanded);
            }
            else if(Child->Value.Type == VALUE_SYMBOL)
            {
               sexp_node* Replacement = dict_get(Args, Child->Value.Ptr);
               sb_push(Out->Children, Replacement ? Replacement : Child);
            }
            else if(Child->Value.Type == VALUE_CINLINE)
            {
               sb_push(Out->Children, RewriteCin(Module, Child, Args));
            }
            else
            {
               sb_push(Out->Children, Child);
            }
         }

         dict_free(Args);
         return Out;
      }
   }

   return NULL;
}

sexp_node* FuncallCinSexp(module* Module, sexp_node* Sexp)
{
   sexp_node* Cin = AllocNode(Module, VALUE_CINLINE);

   DECL_BUF(GeneratedCode, 256);
   WRITE_BUF(GeneratedCode, Sexp->Children[0]->Value.Ptr);
   WRITE_BUF(GeneratedCode, "(");
   if (sb_count(Sexp->Children) > 1)
   {
      // Expand into a function call:
      for (int j=1; j<sb_count(Sexp->Children) - 1; j++)
      {
         sexp_node* Arg = Sexp->Children[j];
         PushArg(Arg, GeneratedCode, &OffsetGeneratedCode);
         WRITE_BUF(GeneratedCode, ", ");
      }
      PushArg(sb_last(Sexp->Children), GeneratedCode, &OffsetGeneratedCode);
   }
   WRITE_BUF(GeneratedCode, ");\n ");

   Cin->Value.Ptr = CreateSymbol(Module->Compiler, GeneratedCode, OffsetGeneratedCode);
   free(GeneratedCode);
   return Cin;
}

sexp_node* EvalHelperSexp(module* Module, uint8 HelperType, sexp_node* HelperSexp);
sexp_node* EvalSexp(module* Module, sexp_node* Sexp)
{
   /* If the node is not a Sexp, return NULL */
   if (sb_count(Sexp->Children) == 0)
   {
      return NULL;
   }

   // Allocate output from processing of this Sexp
   sexp_node* Out = AllocNode(Module, VALUE_SEXP);
   sexp_node *Expanded, *Reevaluated;
   uint8 exec = SexpLogic(Module, Sexp);
   switch (exec)
   {
      case EXEC_FUNCALL:
         Expanded = FuncallCinSexp(Module, Sexp);
         AppendChild(Out, Expanded);
         return Out;
         break;
   }

   /* Traverse all children of the node */
   for (int i=0; i<sb_count(Sexp->Children); i++)
   {
      sexp_node* Child = Sexp->Children[i];

      /* If child is an unquoted SEXP */
      if (Child->Value.Type == VALUE_SEXP && Child->Value.Hnd == 0)
      {
         uint8 exec = SexpLogic(Module, Child);

         // How to process this child further
         switch (exec)
         {
            case EXEC_STOP:
               continue;
               break;
            case EXEC_HELPER:
               Expanded = EvalHelperSexp(Module, FindHelper(Child->Children[0]->Value.Ptr), Child);
               if (Expanded)
               {
                  AppendNode(Out, Expanded);
               }
               break;
            case EXEC_MACRO:
               Expanded    = RewriteSexp(Module, Child);
               Reevaluated = EvalSexp(Module, Expanded);
               if (sb_count(Reevaluated->Children))
               {
                  AppendNode(Out, Reevaluated);
               }
               break;
            case EXEC_RECURSE:
               Expanded = EvalSexp(Module, Child);
               if (sb_count(Expanded->Children) > 0)
               {
                  sb_push(Out->Children, Expanded);
               }
               break;
            case EXEC_FUNCALL:
               Expanded = FuncallCinSexp(Module, Child);
               AppendChild(Out, Expanded);
               break;
         }
      }
      else if (Child->Value.Type == VALUE_SEXP && Child->Value.Hnd == 1) 
      {
         sexp_node* Copy = CopyNode(Module, Child);
         sb_push(Out->Children, Copy);
      }
      else
      {
         sb_push(Out->Children, Child);
      }
   }
   return Out;
}

void OpHeader(module* Module, sexp_node* Sexp)
{
   if (Sexp->Children[1]->Value.Type == VALUE_CINLINE)
   {
      snprintf(Module->HeaderCode, 2*1024, "%s", Sexp->Children[1]->Value.Ptr); 
   }
}

sexp_node* EvalHelperSexp(module* Module, uint8 HelperType, sexp_node* HelperSexp)
{
   sexp_node* Out, *Expanded, *Reevaluated;
   switch (HelperType)
   {
      case HELPER_DEFMACRO:
         OpDefMacro(Module, HelperSexp);
         return NULL;
      case HELPER_DEFUN:
         OpDefun(Module, HelperSexp);
         return NULL;
      case HELPER_CIN:
         Out = OpCin(Module, HelperSexp);
         return Out;
      case HELPER_STRUCT:
         OpStruct(Module, HelperSexp);
         return NULL;
      case HELPER_IMPORT:
         OpImport(Module, HelperSexp);
         return NULL;
      case HELPER_MODULE:
         OpModule(Module, HelperSexp);
         return NULL;
      case HELPER_PRINT:
         OpPrint(Module, HelperSexp);
         return NULL;
      case HELPER_HEADER:
         OpHeader(Module, HelperSexp);
         return NULL;
      case HELPER_FOR:
         Expanded = OpFor(Module, HelperSexp);
         if (Expanded == NULL)
            return NULL;
         Reevaluated = EvalSexp(Module, Expanded);
         return Reevaluated;
      case HELPER_FOREACH:
         Expanded = OpForeach(Module, HelperSexp);
         if (Expanded == NULL)
            return NULL;
         Reevaluated = EvalSexp(Module, Expanded);
         return Reevaluated;
      case HELPER_CASE:
         Expanded    = OpCase(Module, HelperSexp);
         if (Expanded == NULL)
            return NULL;
         Reevaluated = EvalSexp(Module, Expanded);
         return Reevaluated;
      case HELPER_CAPTURE:
         Expanded    = OpCapture(Module, HelperSexp);
         Out = AllocNode(Module, VALUE_SEXP);
         AppendChild(Out, Expanded);
         Reevaluated = EvalSexp(Module, Out);
         return Reevaluated;
      case HELPER_CAPTUREN:
         Expanded    = OpCaptureN(Module, HelperSexp);
         Out = AllocNode(Module, VALUE_SEXP);
         AppendChild(Out, Expanded);
         Reevaluated = EvalSexp(Module, Out);
         return Reevaluated;
      case HELPER_AND:
         Out = OpAnd(Module, HelperSexp);
         return Out;
      case HELPER_OR:
         Out = OpOr(Module, HelperSexp);
         return Out;
      case HELPER_LIST:
         Out = OpList(Module, HelperSexp);
         return Out;
   }
   return NULL;
}


sexp_node* CompileSexp(compiler* Compiler, module* Module, const char* Source, size_t SourceLen, size_t* SexpLen)
{
    token* Tokens = ParseTokens(Source, SourceLen, SexpLen);

    uint16_t TokenStart, TokenEnd;
    int TokenLen;

    uint16_t NumTokens = sb_count(Tokens);

    bool QuoteToken = false;
    sexp_node* CurrentSexp = AllocNode(Module, VALUE_SEXP);
    sexp_node* NewNode = NULL;

    sexp_node* NodeAtLevel[1024];
    memset(NodeAtLevel, 0, sizeof(NodeAtLevel));
    NodeAtLevel[0] = CurrentSexp;
    int32 Level = 1;

    for (int i=0; i<NumTokens; i++)
    {
       switch (Tokens[i].Type)
       {
          case TOKEN_QUOTE:
                QuoteToken = true;
                break;
          case TOKEN_SEXP_START:
                NewNode = AllocNode(Module, VALUE_SEXP);
                NewNode->Value.Hnd = QuoteToken ? 1 : 0;
                AppendChild(CurrentSexp, NewNode);
                NodeAtLevel[Level] = NewNode;
                Level++;
                CurrentSexp = NewNode;
                QuoteToken = false;
                break;
            case TOKEN_SEXP_END:
                Level--;
                if (Level >= 1)
                {
                   CurrentSexp = NodeAtLevel[Level-1];
                }
                break;
            case TOKEN_SYMBOL_END:
                TokenEnd = Tokens[i].Loc;
                TokenLen = TokenEnd - TokenStart;
                NewNode = AllocNode(Module, VALUE_SYMBOL);
                NewNode->Value.Ptr = CreateSymbol(Compiler, Source+TokenStart, TokenLen);
                AppendChild(CurrentSexp, NewNode);
                break;
            case TOKEN_STRING_START:
            case TOKEN_CINLINE_START:
                TokenStart = Tokens[i].Loc + 1;
                QuoteToken = false;
                break;
            case TOKEN_STRING_END:
                TokenEnd = Tokens[i].Loc;
                TokenLen = TokenEnd - TokenStart;
                NewNode = AllocNode(Module, VALUE_STRING);
                NewNode->Value.Ptr = CreateSymbol(Compiler, Source+TokenStart, TokenLen);
                AppendChild(CurrentSexp, NewNode);
                break;
            case TOKEN_SYMBOL_START:
            case TOKEN_INT_START:
            case TOKEN_FLOAT_START:
                TokenStart = Tokens[i].Loc;
                QuoteToken = false;
                break;
            case TOKEN_INT_END:
                TokenEnd = Tokens[i].Loc;
                char ParseBuffer[16];
                memset(ParseBuffer, 0, sizeof(ParseBuffer));
                memcpy(ParseBuffer, Source+TokenStart, TokenEnd-TokenStart);
                int64 ParsedI64 = strtol(ParseBuffer, NULL, 10);
                NewNode = AllocNode(Module, VALUE_I64);
                NewNode->Value.I64 = ParsedI64;
                AppendChild(CurrentSexp, NewNode);
                break;
            case TOKEN_FLOAT_END:
                TokenEnd = Tokens[i].Loc;
                memset(ParseBuffer, 0, sizeof(ParseBuffer));
                memcpy(ParseBuffer, Source+TokenStart, TokenEnd-TokenStart);
                float64 ParsedF64 = strtol(ParseBuffer, NULL, 10);
                NewNode = AllocNode(Module, VALUE_F64);
                NewNode->Value.F64 = ParsedF64;
                AppendChild(CurrentSexp, NewNode);
                break;
            case TOKEN_CINLINE_END:
                TokenEnd = Tokens[i].Loc;
                TokenLen = TokenEnd - TokenStart;
                NewNode = AllocNode(Module, VALUE_CINLINE);
                NewNode->Value.Ptr = CreateSymbol(Compiler, Source+TokenStart, TokenLen);
                AppendChild(CurrentSexp, NewNode);
                break;
        }
    }

   sb_free(Tokens);

   sexp_node* Evaluated = EvalSexp(Module, CurrentSexp);
   return Evaluated;
}

void PushArg(sexp_node* Arg, char* Buf, uint32* BufOffset)
{
   size_t Len = 0;
   char* ArgPtr = Arg->Value.Ptr;
   int64 ArgI64 = Arg->Value.I64;
   float64 ArgF64 = Arg->Value.F64;
   char ConvBuf[20];
   switch (Arg->Value.Type)
   {
      case VALUE_SYMBOL:
      Len = strlen(Arg->Value.Ptr);
      memcpy(Buf + *BufOffset, ArgPtr, Len);
      *BufOffset += Len;
      break;
      case VALUE_STRING:
      Len = strlen(Arg->Value.Ptr);
      memcpy(Buf + *BufOffset, "\"", 1);
      memcpy(Buf + *BufOffset + 1, ArgPtr, Len);
      memcpy(Buf + *BufOffset + Len + 2, "\"", 1);
      *BufOffset += Len + 2;
      break;
      case VALUE_I64:
      snprintf(ConvBuf, 20, "%ld", ArgI64);
      memcpy(Buf + *BufOffset, ConvBuf, strlen(ConvBuf));
      *BufOffset += strlen(ConvBuf);
      break;
      case VALUE_F64:
      snprintf(ConvBuf, 20, "%f", ArgF64);
      memcpy(Buf + *BufOffset, ConvBuf, strlen(ConvBuf));
      *BufOffset += strlen(ConvBuf);
      break;
   }
}

void CompileSource(compiler* Compiler, module* Module, const char* Source, size_t SourceLen)
{
   sexp_node* Sexps = AllocNode(Module, VALUE_SEXP);
   uint32 LastCinStart = 0;
   const size_t MaxGeneratedCodeSize = 32*1024;
   DECL_BUF(GeneratedCode, MaxGeneratedCodeSize);
   for (int i=0; i<SourceLen; i++)
   {
      // Macro expansion
      if (Source[i] == '#' && (Source[i+1] == '(' || Source[i+1] == '\''))
      {
         size_t SexpLen = 0;
         sexp_node* Compiled = CompileSexp(Compiler, Module, Source + i + 1, SourceLen - i, &SexpLen);
         if (SexpLen == 0)
         {
            printf("Error: Sexp %s is not enclosed within parantheses", Source+i);
            return;
         }
         // Every Sexp generated from CompileSexp is enclosed within a Sexp
         if (sb_count(Compiled->Children))
         {
            //PrintSexp(Compiled, 0);
            AppendNode(Sexps, Compiled);
         }

         // Search from the last place we copied from
         // Don't include spaces/newlines in the generated code
         for (int j=LastCinStart; j < i-3; j++)
         {
            if (Source[j] != '\n' || Source[j] != ' ' || Source[j] != '\t')
            {
               // Only generate(include) C code if there is indeed C code
               WRITE_BUFN(GeneratedCode, Source + LastCinStart, i - LastCinStart);
               break;
            }
         }

         i += SexpLen + 2;
         LastCinStart = i;

         // If macros through CompileSexp have generated new code, include them
         if (Compiled->Children)
         {
            for (int ii=0; ii<sb_count(Compiled->Children); ii++)
            {
               // If this children is a leaf node
               if (Compiled->Children[ii]->Children == NULL)
               {
                  // If this leaf node is a CINLINE, include it in the generated code
                  if (Compiled->Children[ii]->Value.Type == VALUE_CINLINE)
                  {
                     WRITE_BUF(GeneratedCode, Compiled->Children[ii]->Value.Ptr);
                     WRITE_BUF(GeneratedCode, "\n");
                  }
                  continue;
               }
               
               // If node has more children, check if the Sexp is a function execution
               else
               {
                  // If the Sexp is not a function call, traverse it's children and include CINLINES
                  for (int j=0; j<sb_count(Compiled->Children[ii]->Children); j++)
                  {
                     if (Compiled->Children[ii]->Children[j]->Value.Type == VALUE_CINLINE) // For OpCapture output
                     {
                        WRITE_BUF(GeneratedCode, Compiled->Children[ii]->Children[j]->Value.Ptr);
                     }
                  }
               }
            }
         }
      }
   }

   // Include the last bits of generated code
   if (LastCinStart != SourceLen)
   {
      WRITE_BUFN(GeneratedCode, Source + LastCinStart, SourceLen - LastCinStart);
   }

   Module->InlineCode = GeneratedCode;
   Module->OffsetInlineCode = OffsetGeneratedCode;
}

char* GetModuleSource(module* Module)
{
   const size_t MaxCodeSize = 64*1024;
   DECL_BUF(ModuleCode, MaxCodeSize);
   WRITE_BUFN(ModuleCode, Module->StructDecls, Module->OffsetStructDecls);
   WRITE_BUFN(ModuleCode, Module->StructDefs, Module->OffsetStructDefs);
   WRITE_BUFN(ModuleCode, Module->FuncDecls, Module->OffsetFuncDecls);
   WRITE_BUFN(ModuleCode, Module->FuncDefs, Module->OffsetFuncDefs);
   WRITE_BUFN(ModuleCode, Module->InlineCode, Module->OffsetInlineCode);
   WRITE_BUF(ModuleCode, "#endif");
   return ModuleCode;
}

bool DumpModuleToDisk(module* Module, const char* OutDir)
{
   char OutFilename[64];
   snprintf(OutFilename, 64, "%s/%s.h", OutDir, Module->Name);
   FILE* OutFile = fopen(OutFilename, "w");

   if (!OutFile)
   {
      printf("Error: Could not open %s for outputting the generated code\n", OutFilename);
      return false;
   }

   const size_t MaxCodeSize = 64*1024;
   DECL_BUF(HeaderCode, MaxCodeSize);
   WRITE_BUF(HeaderCode, "#ifndef MODULE_");
   WRITE_BUF(HeaderCode, Module->Name);
   WRITE_BUF(HeaderCode, "_INCLUDED\n");
   WRITE_BUF(HeaderCode, "#define MODULE_");
   WRITE_BUF(HeaderCode, Module->Name);
   WRITE_BUF(HeaderCode, "_INCLUDED\n");
   WRITE_BUF(HeaderCode, Module->HeaderCode);
   WRITE_BUFN(HeaderCode, Module->StructDecls, Module->OffsetStructDecls);
   WRITE_BUFN(HeaderCode, Module->StructDefs, Module->OffsetStructDefs);
   WRITE_BUFN(HeaderCode, Module->FuncDecls, Module->OffsetFuncDecls);
   WRITE_BUF(HeaderCode, "#endif");

   fwrite(HeaderCode, strlen(HeaderCode), 1, OutFile);
   fclose(OutFile);
   free(HeaderCode);

   snprintf(OutFilename, 64, "%s/%s.c", OutDir, Module->Name);
   OutFile = fopen(OutFilename, "w");

   if (!OutFile)
   {
      printf("Error: Could not open %s for outputting the generated code\n", OutFilename);
      return false;
   }

   DECL_BUF(ModuleCode, MaxCodeSize);
   WRITE_BUF(ModuleCode, "#include \"");
   WRITE_BUF(ModuleCode, Module->Name);
   WRITE_BUF(ModuleCode, ".h\"\n");
   for (int i=0; i<sb_count(Module->Imports); i++)
   {
      WRITE_BUF(ModuleCode, "#include \"");
      WRITE_BUF(ModuleCode, Module->Imports[i]->Name);
      WRITE_BUF(ModuleCode, ".h\"\n");
   }
   WRITE_BUFN(ModuleCode, Module->InlineCode, Module->OffsetInlineCode);
   WRITE_BUFN(ModuleCode, Module->FuncDefs, Module->OffsetFuncDefs);

   fwrite(ModuleCode, strlen(ModuleCode), 1, OutFile);
   fclose(OutFile);
   free(ModuleCode);

   return true;
}

module* LoadModuleSource(compiler* Compiler, const char* Source, size_t SourceLen)
{
   // Free the existing module if there is already
   module* Module = InitModule(Compiler, 32 * 1024);
   CompileSource(Compiler, Module, Source, SourceLen);

   char* ModuleName = Module->Name;

   for (int i=0; i<sb_count(Compiler->Modules); i++)
   {
      if (strcmp(Compiler->Modules[i]->Name, ModuleName) == 0) // If this module has been loaded already, free it
      {
         FreeModule(Compiler->Modules[i]);
         Compiler->Modules[i] = Module;
         return Module;
      }
   }

   sb_push(Compiler->Modules, Module);
   return Module;
}

int main(int Argc, char** Argv)
{
   if (Argc < 2) {
      printf("Usage: yummy [input-file] [output-folder] [module-search-path]\n");
      return 1;
   }

   printf("Compiling file %s\n", Argv[1]);

   const char* Filename = Argv[1];
   char* Source;
   size_t SourceLen, SexpLen;
   LoadFile(Filename, &Source, &SourceLen);

   if (!Source)
      return 1;

   compiler* Compiler = InitCompiler(128 * 1024 * 1024);
   if (Argc > 3)
   {
      CompilerModulePath(Compiler, Argv[3]);
   }
   module* Module = LoadModuleSource(Compiler, Source, SourceLen);
   // Write the output to the file:
   if (Module)
   {
      bool Success = DumpModuleToDisk(Module, Argv[2]);
   }

   free(Source);
   FreeCompiler(Compiler);
   return 0;
}
