CFLAGS = -g -DDEBUG
#CFLAGS = -Os

all: build examples uncrustify

SOURCES = src/yummy.c \
		  src/util.c \
		  src/xxhash.c

OUT_FOLDER = gen
MODULE_SEARCH_FOLDER = examples
DEBUG_MODULE = examples/helpers.ym $(OUT_FOLDER) $(MODULE_SEARCH_FOLDER)

build:
	gcc $(CFLAGS) -std=c99 $(SOURCES) -o yummy

valgrind:
	valgrind --leak-check=full --show-leak-kinds=all ./yummy $(DEBUG_MODULE)

cachegrind:
	valgrind --tool=callgrind --simulate-cache=yes ./yummy $(DEBUG_MODULE)

dbg:
	gdb --args ./yummy $(DEBUG_MODULE)

examples: build
	./yummy examples/helpers.ym $(OUT_FOLDER) $(MODULE_SEARCH_FOLDER)
	./yummy examples/gfx.ym $(OUT_FOLDER) $(MODULE_SEARCH_FOLDER)
	./yummy examples/gfx_gles2.ym $(OUT_FOLDER) $(MODULE_SEARCH_FOLDER)
	./yummy examples/gfx_gles3.ym $(OUT_FOLDER) $(MODULE_SEARCH_FOLDER)

uncrustify:
	find $(OUT_FOLDER) -name "*.c" > .files
	uncrustify -c uncrustify.cfg --no-backup -F .files
	rm .files
