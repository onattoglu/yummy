Yummy
=====
Yummy is a C preprocessor using LISP style macros to generate code.
The idea is to bring low-level programming with C and high level languages created with LISP macros together to create abstractions.

It is recommended to use a prettifier on the output code, such as uncrustify.

Please keep in mind that this is WIP, so ops could change a lot, depending on my needs.

Usage
-----
Yummy sources are regular C files, with macro definitions/expansions embedded inside the source with *#()*.

*#(sexp-op sexp-args)* evaluates a Sexp inside the source, which can:

  * Generate a function call in C, if sexp-op is a function defined with defun.
  * Expand as a macro if sexp-op is a macro defined with defmacro 
  * Expand as a predefined helper macro(documented in examples/helpers.ym) if sexp-op is a helper macro
  * Do nothing - expands to nothing in the generated code
  
To compile a Yummy source to C use

    yummy [in-filename] [out-folder]
